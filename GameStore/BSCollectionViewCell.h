//
//  BSCollectionViewCell.h
//  GameStore
//
//  Created by Brian Strobach on 4/22/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImageView;


-(void)setImage:(UIImage *)image;
@end
