//
//  BSCollectionViewCell.m
//  GameStore
//
//  Created by Brian Strobach on 4/22/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import "BSCollectionViewCell.h"

@implementation BSCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)setImage:(UIImage *)image
{
    if (!_thumbnailImageView) {
        _thumbnailImageView = [[UIImageView alloc]init];
    }
    [_thumbnailImageView setImage:image];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
