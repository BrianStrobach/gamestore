//
//  BSCollectionViewController.h
//  GameStore
//
//  Created by Brian Strobach on 4/21/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSCollectionViewController : UICollectionViewController <NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSString *searchTerm;

@end
