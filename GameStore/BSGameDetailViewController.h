//
//  BSGameDetailViewController.h
//  GameStore
//
//  Created by Brian Strobach on 4/22/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSGameDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@end
