//
//  BSGameDetailViewController.m
//  GameStore
//
//  Created by Brian Strobach on 4/22/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import "BSGameDetailViewController.h"
#import "Game.h"
#import "StoreManagerCloud.h"

@interface BSGameDetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *appNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *developerNameLabel;
@property (strong, nonatomic) IBOutlet UITextView *appDescriptionTextView;
@property (strong, nonatomic) Game *game;

@end

@implementation BSGameDetailViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    _game = (Game *)_detailItem;
    _appNameLabel.text = _game.appName;
    [_appNameLabel sizeToFit];
    _developerNameLabel.text = _game.developerName;
    [_developerNameLabel sizeToFit];
    _appDescriptionTextView.text = _game.appDescription;
    [_appDescriptionTextView sizeToFit];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buyInAppstoreButtonPressed:(UIButton *)sender {
    
    NSURL *appURL = [[NSURL alloc]init];
    appURL = [NSURL URLWithString:_game.appStoreURL];

    NSLog(@"%@", appURL.path);
    [[UIApplication sharedApplication] openURL: appURL];

}
- (IBAction)addToWishlistButtonPressed:(UIButton *)sender {
    NSManagedObjectContext *context = [StoreManagerCloud sharedStoreManagerCloud].managedObjectContext;
    Game *newGame = [NSEntityDescription insertNewObjectForEntityForName:@"Game"
                                                  inManagedObjectContext:context];
    newGame.appName = _game.appName;
    newGame.appDescription = _game.appDescription;
    newGame.developerName = _game.developerName;
    newGame.artworkURL60 = _game.artworkURL60;
    newGame.appStoreURL = _game.appStoreURL;
    newGame.searchTerms = _game.searchTerms;
    
    [[StoreManagerCloud sharedStoreManagerCloud] saveContext];
    
    [[self navigationController]popViewControllerAnimated:YES];

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
