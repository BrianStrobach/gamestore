//
//  BSSearchViewController.h
//  GameStore
//
//  Created by Brian Strobach on 4/23/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSSearchViewController : UIViewController <UISearchBarDelegate>

@end
