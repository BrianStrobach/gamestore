//
//  main.m
//  GameStore
//
//  Created by Brian Strobach on 4/19/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSAppDelegate class]));
    }
}
