//
//  WishlistTableViewController.h
//  GameStore
//
//  Created by Brian Strobach on 4/23/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WishlistTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
